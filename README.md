1.  Game possède Iterateur. Si le lien entre Game et Iterateur n'est pas une possèssion, 
    l'iterateur sera perdu après chaque exécution d'un tour de table.
    La possession permet donc de créer un seul itérateur qui sera utilisé tout au long du jeux.
    Cette conception implique une méthode reset() dans Iterateur qui remet le pointeur à 0.
    Reset() peut être appellé automatiquement dans la méthode hasNext() lorsqu'elle retourne false.