package frameWork;

/**
 * Defines the common API an iterable collection has to implement.
 * It provides methods to add an item, get the size of the collection and create the associated iterator.
 *
 * @param <T> Any type.
 */
public interface IterableCollection<T> {

	/**
	 * Creates the associated iterator allowing to iterate over the collection.
	 *
	 * @return The iterator, with its cursor at the first place.
	 */
	Iterator<T> createIterator();

	/**
	 * Add the item to the collection.
	 *
	 * @param item The object to store in the collection.
	 */
	void addItem(T item);

	/**
	 * Returns the actual count of non-null item in the collection.
	 *
	 * @return The exact amount of non-null objects in the collection.
	 */
	int size();
}
