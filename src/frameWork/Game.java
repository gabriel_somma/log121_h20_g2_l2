package frameWork;

public class Game {

    private int actualTurn;
    private int turnNb;
    private PlayerCollection PlayerCollection;
    private DiceCollection DiceCollection;
    private Iterator<Dice> diceIterator;
    private Iterator<Player> playerIterator;
    private Rule gameRule;

    public Game(frameWork.DiceCollection dices, frameWork.PlayerCollection players, Rule rules, int turnNb) {
        this.DiceCollection = dices;
        this.PlayerCollection = players;
        this.gameRule = rules;
        this.turnNb = turnNb;

        this.diceIterator = this.DiceCollection.createIterator();
        this.playerIterator = this.PlayerCollection.createIterator();
        this.actualTurn = 1;
    }

    private void doOneTurn() {

        // Player who's turn it is to play
        Player actualPlayer;
        // The dice that will be cast by the player
        Dice aDice;

        // Iterate over players Collection
        while (playerIterator.hasNext()) {
            actualPlayer = playerIterator.next();

            // this do while allows the player to play again depending on the rules
            do {

                // Iterate over dice Collection
                while (diceIterator.hasNext()) {
                    aDice = diceIterator.next();
                    actualPlayer.castDice(aDice);
                }

                this.gameRule.turnScoreCalculator(this, actualPlayer);

            } while (this.gameRule.playAgain(this));
        }
    }

    private void play() {
        while (this.turnNb <= this.actualTurn) {
            doOneTurn();
            this.actualTurn++;
        }
        System.out.println("Winner is : " + this.gameRule.winnerCalculator(this));
    }
}
