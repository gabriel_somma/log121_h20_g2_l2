package frameWork;

/******************************************************
Cours:  LOG121
Session: H2020
Groupe:  02
Projet: Laboratoire #2
Étudiant(e)s: Bradley Henry How Chen Nian
              
Professeur : Vincent Lacasse
Nom du fichier: Dice.java
Date crée: 2020-03-02
Date dern. modif. 2020-03-02
*******************************************************
Historique des modifications
*******************************************************
2013-03-02 Version initiale (et1)
*******************************************************/  

/**
 * The class Dice is a class to simulate a dice with 2 faces or higher.
 * 
 * @author Bradley Henry How Chen Nian
 *
 */
public class Dice implements Comparable<Dice> {
	static final int DEFAULTNBFACE = 6;
	// Number of face of the dice
	private int faceNb;
	
	// Result at the top of dice
	private int topFaceNb;
	
	/**
	 * Default constructor of dice 
	 */
	public Dice() {
		this.faceNb = DEFAULTNBFACE;
	}
	/**
	 * Constructor of dice
	 * @param faceNb need to be higher than 1
	 */
	public Dice(int faceNb) {
		if (faceNb <= 1) {
			throw new IllegalArgumentException("Number of face need to be higher than 1");
		}
		this.faceNb = faceNb;
	}
	
	/**
	 * Function that throw the dice
	 * @return value at the top face of the dice
	 */
	public int shake() {
		topFaceNb = (int) (Math.random() * faceNb + 1);
		return topFaceNb; 
	}
	
	/**
	 * setter of the top face of the dice
	 * @param number need to be in the range of 1 to faceNb
	 */
	public void setFaceTo(int number) {
		if (number < 1 || number > faceNb) {
			throw new IllegalArgumentException("Number of face need to be 1 to " + this.faceNb);
		}
		this.topFaceNb = number;
	}
	
	/**
	 * Function to compare this Dice and another Dice o
	 * @return 	-1 if the top of this dice is inferior than the top of dice o , 
	 * 			 0 if the top of this dice is equal than the top of dice o,
	 * 			 1 if the top of this dice is superior than the top of dice o 
	 */
	public int compareTo(Dice o) {
		if (o == null) {
			throw new IllegalArgumentException("Object o cannot be null");
		}else if (this.topFaceNb > o.topFaceNb) {
			return 1;
		} else if (this.topFaceNb < o.topFaceNb) {
			return -1;
		}
		return 0;
	}

}
