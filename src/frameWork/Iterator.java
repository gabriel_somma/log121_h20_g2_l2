package frameWork;

/**
 * Defines the common API the iterators must implement.
 * It defines a method to get the next element, check if there is a next element and reset the cursor's position.
 *
 * @param <T> Any type.
 */
public interface Iterator<T> {

    /**
     * Gets the next element. If there is no next element it returns null.
     *
     * @return The next element if there is one, null otherwise.
     */
    T next();

    /**
     * Check if there is another element to iterate over.
     *
     * @return True if the cursor isn't at the last position, false otherwise.
     */
    boolean hasNext();

    /**
     * Reset the cursor's position to the first element.
     */
    void reset();
}
