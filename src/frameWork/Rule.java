package frameWork;

public interface Rule {

 void turnScoreCalculator(Game game, Player actualPlayer);
 Player winnerCalculator(Game game);
 boolean playAgain(Game game);
}