package frameWork;

/**
 * Represents the collection of players used in the Game.
 *
 * @author Quentin Rimoux
 * @see Player
 * @since 1.0
 */
public class PlayerCollection implements IterableCollection<frameWork.Player> {

    /**
     * Buffer size for the inner array so it doesn't have to be created again each time a dice is added.
     */
    private static final int BUFFER_SIZE = 5;

	/**
	 * Array containing the players. Only way to access it is using the PlayerIterator.
	 */
	private Player[] players;

	/**
	 * Constructor initializing the inner array to a default size of BUFFER_SIZE elements.
	 */
    public PlayerCollection() {
        players = new Player[BUFFER_SIZE];
    }

    @Override
    public Iterator<Player> createIterator() {
        return new PlayerIterator();
    }

    @Override
    public void addItem(frameWork.Player item) {
        if (item == null)
            throw new IllegalArgumentException();
        if (players[players.length - 1] != null) {

            Player[] tmp = new Player[players.length + BUFFER_SIZE];
            System.arraycopy(players, 0, tmp, 0, players.length);
            players = tmp;
        }

        for (int i = 0; i < players.length; i++) {
            if (players[i] == null) {
                players[i] = item;
                return;
            }
        }
    }

    @Override
    public int size() {
        return (int) java.util.stream.IntStream.range(0, players.length).filter(i -> players[i] != null).count();
    }

	/**
	 * Iterator for the PlayerCollection.
	 *
	 * @author Quentin Rimoux
	 * @version 1.0
	 * @see PlayerCollection
	 * @see Player
	 */
    private class PlayerIterator implements Iterator<Player> {

        /**
         * Current position in the iterated DiceCollection.
         */
        int cursor;

		/**
		 * Constructor initializing the cursor's position to 0.
		 */
		private PlayerIterator() {
            reset();
        }

        @Override
        public Player next() {
            Player player = null;
            if (hasNext()) {
                player = players[cursor];
                cursor++;
            }

            return player;
        }

        @Override
        public boolean hasNext() {
            return cursor < size();
        }

        @Override
        public void reset() {
            cursor = 0;
        }
    }
}
