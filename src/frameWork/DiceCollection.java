package frameWork;

/**
 * Represents a set of dice used by the Game instance.
 *
 * @author Quentin Rimoux
 * @see Dice
 * @since 1.0
 */
public class DiceCollection implements IterableCollection<Dice> {

    /**
     * Buffer size for the inner array so it doesn't have to be created again each time a dice is added.
     */
    private static final int BUFFER_SIZE = 5;


    /**
     * Array containing the actual dices. Only way to access it is by using the iterator.
     */
    private Dice[] dices;

    public DiceCollection() {
        dices = new Dice[BUFFER_SIZE];
    }


    @Override
    public Iterator<Dice> createIterator() {
       return new frameWork.DiceCollection.DiceIterator();
    }

    @Override
    public void addItem(Dice item) {
        if (item == null)
            throw new IllegalArgumentException();
        if (dices[dices.length - 1] != null) {

            Dice[] tmp = new Dice[dices.length + BUFFER_SIZE];
            System.arraycopy(dices, 0, tmp, 0, dices.length);
            dices = tmp;
        }

        for (int i = 0; i < dices.length; i++) {
            if (dices[i] == null) {
                dices[i] = item;
                return;
            }
        }
    }

    @Override
    public int size() {
        return (int) java.util.stream.IntStream.range(0, dices.length).filter(i -> dices[i] != null).count();
    }

    /**
     * Iterator for the DiceCollection.
     *
     * @author Quentin Rimoux
     * @version 1.0
     * @see DiceCollection
     * @see Dice
     */
    private class DiceIterator implements frameWork.Iterator<Dice> {

        /**
         * Current position in the iterated DiceCollection.
         */
        int cursor;

        private DiceIterator() {
            reset();
        }

        @Override
        public frameWork.Dice next() {
            Dice dice = null;
            if (hasNext()) {
                dice = dices[cursor];
                cursor++;
            }

            return dice;
        }

        @Override
        public boolean hasNext() {
            return cursor < size();
        }

        @Override
        public void reset() {
            cursor = 0;
        }
    }
}
