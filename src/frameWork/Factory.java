package frameWork;

public class Factory {

    private Game createGame(DiceCollection dices, PlayerCollection players, Rule rules, int turnNb) {
        return new Game(dices, players, rules, turnNb);
    }
}
