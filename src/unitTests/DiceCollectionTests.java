package unitTests;

import frameWork.*;
import org.junit.*;


import static org.junit.jupiter.api.Assertions.*;

/**
 * Test class for the DiceCollection and its iterator.
 *
 * @author Quentin Rimoux
 * @version 1.0
 */
public class DiceCollectionTests {

    /**
     * DiceCollection instance used for all the tests.
     */
    private DiceCollection dices;

    /**
     * Initialize the DiceCollection used in all tests.
     */
    @Before
    public void beforeTests() {
        dices = new DiceCollection();
    }

    /**
     * Ensure that the DiceCollection is initialized with no element (i.e a size of 0).
     */
    @Test
    public void testDiceCollectionInitialSize() {
        assertEquals(0, dices.size());
    }

    /**
     * Ensure that adding a dice to the collection is working.
     */
    @Test
    public void testDiceCollectionAddItem() {
        dices.addItem(new Dice(6));
        assertEquals(1, dices.size());
    }

    /**
     * Ensure that when the array is full, it is extended properly.
     */
    @Test
    public void testDiceCollectionExtendSize() {
        for (int i = 0; i < 7; i++) {
            dices.addItem(new Dice(6));
        }

        assertEquals(7, dices.size());
    }

    /**
     * Ensure that trying to add a null item throw an IllegalArgumentException.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testDiceCollectionAddNullThrowException() {
        dices.addItem(null);
    }

    /**
     * Ensure that the createIterator() method returns a non-null object.
     */
    @Test
    public void testDiceCollectionCreateIterator() {
        Iterator<Dice> iter = dices.createIterator();
        assertNotNull(iter);
    }

    /**
     * Ensure that the next() method moves correctly the cursor and returns the corresponding element.
     */
    @Test
    public void testDiceIteratorNext() {
        dices.addItem(new Dice(6));
        dices.addItem(new Dice(6));
        Iterator<Dice> iter = dices.createIterator();
        Dice dice = iter.next();
        assertNotNull(dice);
        dice = iter.next();
        assertNotNull(dice);
    }

    /**
     * Ensure that when the iterator is beyond the last element it returns null.
     */
    @Test
    public void testDiceIteratorReturnNullWhenReachedTheEnd() {
        dices.addItem(new Dice(6));
        Iterator<Dice> iter = dices.createIterator();
        iter.next();
        Dice dice = iter.next();
        assertNull(dice);
    }

    /**
     * Ensure that the hasNext() method returns true when the collection has still elements to iterate over, false
     * otherwise.
     */
    @Test
    public void testDiceIteratorHasNext() {
        dices.addItem(new Dice(6));
        Iterator<Dice> iter = dices.createIterator();
        assertTrue(iter.hasNext());
        iter.next();
        assertFalse(iter.hasNext());
    }

    /**
     * Ensure that the reset() method puts the cursor back to the first position.
     */
    @Test
    public void testDiceIteratorReset() {
        dices.addItem(new Dice(6));
        Iterator<Dice> iter = dices.createIterator();
        iter.next();
        iter.reset();
        assertTrue(iter.hasNext());
    }
}
