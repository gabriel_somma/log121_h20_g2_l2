package unitTests;

import org.junit.*;


import static org.junit.jupiter.api.Assertions.*;


import frameWork.Dice;

/******************************************************
Cours:  LOG121
Session: H2020
Groupe:  02
Projet: Laboratoire #2
Étudiant(e)s: Bradley Henry How Chen Nian
              
Professeur : Vincent Lacasse
Nom du fichier: Dice.java
Date crée: 2020-03-02
Date dern. modif. 2020-03-02
*******************************************************
Historique des modifications
*******************************************************
2013-03-02 Version initiale (et1)
*******************************************************/  


/**
 * The tests of the class Dice
 * 
 *
 * @author Bradley Henry How Chen Nian
 *
 */

public class DiceTests {
	private Dice dice1;
	private Dice dice2;

	/**
	 * Before the class, initialize two Dices
	 */
	@Before
	public void beforeTesting(){
		dice1 = new Dice(6);
		dice2 = new Dice(6);
	}
	
	/**
	 * Test of constructor dice with one face 
	 */
	@Test(expected=IllegalArgumentException.class)
	public void constructorOneFace(){
		dice1 = new Dice(1);
	}
	/**
	 * Test of constructor dice with negative number of face 
	 */
	@Test(expected=IllegalArgumentException.class)
	public void constructorNegativeFace(){
		dice1 = new Dice(-10);
	}
	/**
	 * Test of negative face 
	 */
	@Test(expected=IllegalArgumentException.class)
	public void invalidNegativeFaceTest(){
		dice1.setFaceTo(-1);
	}
	/**
	 * Test of 0 face 
	 */
	@Test(expected=IllegalArgumentException.class)
	public void invalidFaceTest(){
		dice1.setFaceTo(0);
	}
	/**
	 * Test of a face higher than the number of face 
	 */
	@Test(expected=IllegalArgumentException.class)
	public void invalidSuperiorFaceTest(){
		dice1.setFaceTo(420);
	}
	/**
	 * Test of function shake
	 */
	@Test
	public void shakeTest(){
		assertTrue(dice1.shake()>1);
	}
	/**
	 * Test of function compareTo with the dice1 inferior than dice2 
	 */
	@Test
	public void ofInferiorTest(){
		dice1.setFaceTo(4);
		dice2.setFaceTo(5);
		assertTrue(dice1.compareTo(dice2)==-1);
	}
	
	/**
	 * Test of function compareTo with the dice1 superior than dice2 
	 */
	@Test
	public void ofSuperiorTest(){
		dice1.setFaceTo(4);
		dice2.setFaceTo(5);
		assertTrue(dice2.compareTo(dice1)==1);
	}

	/**
	 * Test of function compareTo with the dice1 equal than dice2 
	 */
	@Test
	public void equalTest(){
		dice1.setFaceTo(3);
		assertTrue(dice1.compareTo(dice1)==0);
	}
	
	/**
	 * Test of function compareTo with object null
	 */
	@Test(expected=IllegalArgumentException.class)
	public void ofNullTest(){
		dice1.setFaceTo(4);
		dice1.compareTo(null);
	}
}
