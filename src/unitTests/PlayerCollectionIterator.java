package unitTests;

import frameWork.*;
import org.junit.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Test class for the PlayerCollection and its iterator.
 *
 * @author Quentin Rimoux
 * @version 1.0
 */
public class PlayerCollectionIterator {

    // ATTRIBUTES

    /**
     * PlayerCollection used for all the tests.
     */
    private PlayerCollection players;

    // METHODS

    @Before
    public void beforeTests() {
        players = new PlayerCollection();
    }

    /**
     * Ensure that the PlayerCollection is initialized with no element (i.e a size of 0).
     */
    @Test
    public void testPlayerCollectionInitialSize() {
        assertEquals(0, players.size());
    }

    /**
     * Ensure that adding a player to the collection is working.
     */
    @Test
    public void testPlayerCollectionAddItem() {
        players.addItem(new Player());
        assertEquals(1, players.size());
    }

    /**
     * Ensure that when the array is full, it is extended properly.
     */
    @Test
    public void testPlayerCollectionExtendSize() {
        for (int i = 0; i < 7; i++) {
            players.addItem(new Player());
        }

        assertEquals(7, players.size());
    }


    /**
     * Ensure that trying to add a null item throw an IllegalArgumentException.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testPlayerCollectionAddNullThrowException() {
        players.addItem(null);
    }

    /**
     * Ensure that the createIterator() method returns a non-null object.
     */
    @Test
    public void testPlayerCollectionCreateIterator() {
        Iterator<Player> iter = players.createIterator();
        assertNotNull(iter);
    }

    /**
     * Ensure that the next() method moves correctly the cursor and returns the corresponding element.
     */
    @Test
    public void testPlayerIteratorNext() {
        players.addItem(new Player());
        players.addItem(new Player());
        Iterator<Player> iter = players.createIterator();
        Player player = iter.next();
        assertNotNull(player);
        player = iter.next();
        assertNotNull(player);
    }

    /**
     * Ensure that when the iterator is beyond the last element it returns null.
     */
    @Test
    public void testPlayerIteratorReturnNullWhenReachedTheEnd() {
        players.addItem(new Player());
        Iterator<Player> iter = players.createIterator();
        iter.next();
        Player player = iter.next();
        assertNull(player);
    }

    /**
     * Ensure that the hasNext() method returns true when the collection has still elements to iterate over, false
     * otherwise.
     */
    @Test
    public void testPlayerIteratorHasNext() {
        players.addItem(new Player());
        Iterator<Player> iter = players.createIterator();
        assertTrue(iter.hasNext());
        iter.next();
        assertFalse(iter.hasNext());
    }

    /**
     * Ensure that the reset() method puts the cursor back to the first position.
     */
    @Test
    public void testPlayerIteratorReset() {
        players.addItem(new Player());
        Iterator<Player> iter = players.createIterator();
        iter.next();
        iter.reset();
        assertTrue(iter.hasNext());
    }
}
